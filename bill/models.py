#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from django.db import models

class BillAgeements(models.Model):
    agreementId = models.AutoField(db_column='agreementId', primary_key=True)  # Field name made lowercase.
    dateFrom = models.DateField(db_column='dateFrom')  # Field name made lowercase.
    dateTill = models.DateField(db_column='dateTill', blank=True, null=True)  # Field name made lowercase.
    providerId = models.ForeignKey('core.CoreOrganizations', db_column='providerId', related_name='provider')  # Field name made lowercase.
    clientId = models.ForeignKey('core.CoreOrganizations', db_column='clientId', related_name='client')  # Field name made lowercase.
    type = models.CharField(max_length=10)
    printId = models.CharField(db_column='printId', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'bill_ageements'


class BillInvoicecontent(models.Model):
    invoiceId = models.ForeignKey('BillInvoices', db_column='invoiceId')  # Field name made lowercase.
    serviceId = models.ForeignKey('BillServices', db_column='serviceId')  # Field name made lowercase.
    periodFrom = models.DateField(db_column='periodFrom')  # Field name made lowercase.
    periodTill = models.DateField(db_column='periodTill')  # Field name made lowercase.
    price = models.DecimalField(max_digits=20, decimal_places=5)
    count = models.DecimalField(max_digits=20, decimal_places=5)
    name = models.CharField(max_length=255)
    vat = models.SmallIntegerField()
    vatSum = models.DecimalField(db_column='vatSum', max_digits=20, decimal_places=5)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'bill_invoiceContent'


class BillInvoices(models.Model):
    invoiceId = models.AutoField(db_column='invoiceId', primary_key=True)  # Field name made lowercase.
    printId = models.CharField(db_column='printId', max_length=25)  # Field name made lowercase.
    docDate = models.DateField(db_column='docDate')  # Field name made lowercase.
    agreementId = models.ForeignKey(BillAgeements, db_column='agreementId')  # Field name made lowercase.
    periodFrom = models.DateField(db_column='periodFrom')  # Field name made lowercase.
    periodTill = models.DateField(db_column='periodTill')  # Field name made lowercase.
    bankAccountId = models.ForeignKey('core.CoreBankAccounts', db_column='bankAccountId')  # Field name made lowercase.
    createTimestamp = models.IntegerField(db_column='createTimestamp')  # Field name made lowercase.
    validateTimestamp = models.IntegerField(db_column='validateTimestamp', blank=True, null=True)  # Field name made lowercase.
    amountpaid = models.DateField(db_column='amountPaid', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'bill_invoices'


class BillOrdercontent(models.Model):
    orderId = models.ForeignKey('BillOrders', db_column='orderId')  # Field name made lowercase.
    action = models.CharField(max_length=6)
    referenceOrderContentId = models.ForeignKey('self', db_column='referenceOrderContentId', blank=True, null=True)  # Field name made lowercase.
    serviceId = models.ForeignKey('BillServices', db_column='serviceId')  # Field name made lowercase.
    price = models.DecimalField(max_digits=20, decimal_places=5)
    count = models.DecimalField(max_digits=20, decimal_places=5)
    period = models.CharField(max_length=5)
    dateFrom = models.DateField(db_column='dateFrom')  # Field name made lowercase.
    dateTill = models.DateField(db_column='dateTill', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'bill_orderContent'


class BillOrdertypes(models.Model):
    ordertypeId = models.AutoField(db_column='orderTypeId', primary_key=True)  # Field name made lowercase.
    description = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'bill_orderTypes'


class BillOrders(models.Model):
    orderId = models.AutoField(db_column='orderId', primary_key=True)  # Field name made lowercase.
    orderType = models.IntegerField(db_column='orderType')  # Field name made lowercase.
    orderDate = models.DateField(db_column='orderDate')  # Field name made lowercase.
    agreementId = models.ForeignKey(BillAgeements, db_column='agreementId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'bill_orders'


class BillServicemap(models.Model):
    agreementId = models.ForeignKey(BillAgeements, db_column='agreementId')  # Field name made lowercase.
    orderId = models.IntegerField(db_column='orderId', blank=True, null=True)  # Field name made lowercase.
    serviceId = models.ForeignKey('BillServices', db_column='serviceId')  # Field name made lowercase.
    price = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)
    priceMinimal = models.DecimalField(db_column='priceMinimal', max_digits=20, decimal_places=5, blank=True, null=True)  # Field name made lowercase.
    period = models.IntegerField(blank=True, null=True)
    dateFrom = models.DateField(db_column='dateFrom')  # Field name made lowercase.
    dateTill = models.DateField(db_column='dateTill', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'bill_serviceMap'


class BillServices(models.Model):
    serviceId = models.AutoField(db_column='serviceId', primary_key=True)  # Field name made lowercase.
    description = models.CharField(max_length=255)
    moduleId = models.IntegerField(db_column='moduleId', blank=True, null=True)  # Field name made lowercase.
    nameRu = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'bill_services'