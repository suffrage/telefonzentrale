from django.contrib import admin

from models import *

admin.site.register(BillAgeements)
admin.site.register(BillInvoicecontent)
admin.site.register(BillInvoices)
admin.site.register(BillOrdercontent)
admin.site.register(BillOrdertypes)
admin.site.register(BillOrders)
admin.site.register(BillServicemap)
admin.site.register(BillServices)