#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

@python_2_unicode_compatible
class DirReason(models.Model):
    title = models.CharField(db_column='title', max_length=255)

    class Meta:
        db_tablespace = 'dir_reason'

    def __str__(self):
        return self.title


class DirBanks(models.Model):
    bankId = models.AutoField(db_column='bankId', primary_key=True)  # Field name made lowercase.
    bankCountryId = models.ForeignKey('DirCountry', db_column='bankCountryId')  # Field name made lowercase.
    bankName = models.CharField(db_column='bankName', max_length=255)  # Field name made lowercase.
    bankBic = models.CharField(db_column='bankBic', max_length=255)  # Field name made lowercase.
    bankCorrAcc = models.CharField(db_column='bankCorrAcc', max_length=255)  # Field name made lowercase.
    bankAddress = models.CharField(db_column='bankAddress', max_length=255)  # Field name made lowercase.
    bankPhone = models.CharField(db_column='bankPhone', max_length=255)  # Field name made lowercase.
    bankSwift = models.CharField(db_column='bankSWIFT', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'dir_banks'
        verbose_name = 'Банк'
        verbose_name_plural = 'Банки'

    def __unicode__(self):
    	return self.bankName


class DirCity(models.Model):
    cityId = models.AutoField(db_column='cityId', primary_key=True)  # Field name made lowercase.
    regionId = models.ForeignKey('DirRegions', db_column='regionId')  # Field name made lowercase.
    cityName = models.CharField(db_column='cityName', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'dir_city'
        verbose_name = 'Город'
        verbose_name_plural = 'Города'

    def __str__(self):
        return self.cityName


class DirCountry(models.Model):
    countryid = models.AutoField(db_column='countryId', primary_key=True)  # Field name made lowercase.
    countryName = models.CharField(db_column='countryName', max_length=255)  # Field name made lowercase.
    countryIso2 = models.CharField(db_column='countryISO2', max_length=2)  # Field name made lowercase.
    countryIso3 = models.CharField(db_column='countryISO3', max_length=3)  # Field name made lowercase.
    countryIana = models.CharField(db_column='countryIANA', max_length=16)  # Field name made lowercase.
    countryUn = models.CharField(db_column='countryUN', max_length=3)  # Field name made lowercase.
    countryiOc = models.CharField(db_column='countryIOC', max_length=3)  # Field name made lowercase.
    countryun_iso = models.CharField(db_column='countryUN_ISO', max_length=3)  # Field name made lowercase.
    countryItu = models.CharField(db_column='countryITU', max_length=16)  # Field name made lowercase.
    numberLength = models.IntegerField(db_column='numberLength', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'dir_country'

    def __str__(self):
    	return self.countryName


class DirRegions(models.Model):
    regionid = models.AutoField(db_column='regionId', primary_key=True)  # Field name made lowercase.
    countryid = models.ForeignKey(DirCountry, db_column='countryId')  # Field name made lowercase.
    regionname = models.CharField(db_column='regionName', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'dir_regions'