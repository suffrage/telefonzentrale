# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DirBanks',
            fields=[
                ('bankId', models.AutoField(serialize=False, primary_key=True, db_column=b'bankId')),
                ('bankName', models.CharField(max_length=255, db_column=b'bankName')),
                ('bankBic', models.CharField(max_length=255, db_column=b'bankBic')),
                ('bankCorrAcc', models.CharField(max_length=255, db_column=b'bankCorrAcc')),
                ('bankAddress', models.CharField(max_length=255, db_column=b'bankAddress')),
                ('bankPhone', models.CharField(max_length=255, db_column=b'bankPhone')),
                ('bankSwift', models.CharField(max_length=255, db_column=b'bankSWIFT')),
            ],
            options={
                'verbose_name': '\u0411\u0430\u043d\u043a',
                'db_table': 'dir_banks',
                'managed': False,
                'verbose_name_plural': '\u0411\u0430\u043d\u043a\u0438',
            },
        ),
        migrations.CreateModel(
            name='DirCity',
            fields=[
                ('cityid', models.AutoField(serialize=False, primary_key=True, db_column=b'cityId')),
                ('cityname', models.CharField(max_length=255, db_column=b'cityName')),
            ],
            options={
                'verbose_name': '\u0413\u043e\u0440\u043e\u0434',
                'db_table': 'dir_city',
                'managed': False,
                'verbose_name_plural': '\u0413\u043e\u0440\u043e\u0434\u0430',
            },
        ),
        migrations.CreateModel(
            name='DirCountry',
            fields=[
                ('countryid', models.AutoField(serialize=False, primary_key=True, db_column=b'countryId')),
                ('countryName', models.CharField(max_length=255, db_column=b'countryName')),
                ('countryIso2', models.CharField(max_length=2, db_column=b'countryISO2')),
                ('countryIso3', models.CharField(max_length=3, db_column=b'countryISO3')),
                ('countryIana', models.CharField(max_length=16, db_column=b'countryIANA')),
                ('countryUn', models.CharField(max_length=3, db_column=b'countryUN')),
                ('countryiOc', models.CharField(max_length=3, db_column=b'countryIOC')),
                ('countryun_iso', models.CharField(max_length=3, db_column=b'countryUN_ISO')),
                ('countryItu', models.CharField(max_length=16, db_column=b'countryITU')),
                ('numberLength', models.IntegerField(null=True, db_column=b'numberLength', blank=True)),
            ],
            options={
                'db_table': 'dir_country',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DirRegions',
            fields=[
                ('regionid', models.AutoField(serialize=False, primary_key=True, db_column=b'regionId')),
                ('regionname', models.CharField(max_length=255, db_column=b'regionName')),
            ],
            options={
                'db_table': 'dir_regions',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DirReason',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, db_column=b'title')),
            ],
            options={
                'db_tablespace': 'dir_reason',
            },
        ),
    ]
