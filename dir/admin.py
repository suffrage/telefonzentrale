#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from django.contrib import admin

from models import *

class DirBanksAdmin(admin.ModelAdmin):
	list_display = ['bankCountryId', 'bankName', 'bankBic', 
					'bankCorrAcc', 'bankAddress', 'bankPhone', 'bankSwift']
	search_fields = list_display

admin.site.register(DirBanks, DirBanksAdmin)
admin.site.register(DirCity)
admin.site.register(DirCountry)
admin.site.register(DirRegions)
admin.site.register(DirReason)