#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from core.models import CoreOrganizations

try:
    import cStringIO as StringIO
except ImportError:
    import StringIO

from django.http import HttpResponse

import xlsxwriter
import random
import string


def genId():
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(20))

def generatePrice(organization, outputType=True):
	
	if outputType:
		output = StringIO.StringIO()
	else: 
		output = 'lk/static/xlsx/'+genId()+'.xlsx'

	workbook = xlsxwriter.Workbook(output)
	worksheet = workbook.add_worksheet()


	row = 2

	# Settings
	date_format = workbook.add_format({'num_format': 'd mmmm yyyy'})
	algnCenter = workbook.add_format({'align': 'center'})
	headerFormat = workbook.add_format({
	    'bold':     True,
	    'border':   2,
	    'align':    'center',
	    'valign':   'vcenter',
	    'fg_color': '#D7E4BC',
	})
	title = workbook.add_format({
	    'bold':     True,
	    'align':    'center',
	    'valign':   'vcenter',
	    'font_size':	18,
	})

	worksheet.merge_range('B2:I2', u'Тарифное уведомление', title)
	worksheet.merge_range('C3:I3', 'Merged Cells')
	worksheet.set_default_row(15)
	worksheet.set_column('A:A', 1)
	worksheet.set_column('B:C', 23)
	worksheet.set_column('D:E', 11, algnCenter)
	worksheet.set_column('F:G', 10, algnCenter)
	worksheet.set_column('H:H', 10, algnCenter)
	worksheet.set_column('I:I', 18, algnCenter)
	worksheet.set_row(0, 10)
	worksheet.set_row(1, 30)
	worksheet.set_row(2, 140)

	worksheet.freeze_panes(4, 0)
	worksheet.autofilter('B4:C1000')


	# Head
	worksheet.insert_image('B3', 'lk/static/img/logo.png', {'x_scale': 0.1, 
															'y_scale': 0.1,
															'x_offset': 15, 
															'y_offset': 10})
	headerText = u"""
		Общество с ограниченной ответственностью "Хайнцельменхен"
		Heinzelmännchen GmbH
		ОГРН: 1126455002145 	ИНН: 6455056632	КПП: 645201001	ОКПО:37811472
		АдресРФ, 410012, г.Саратов, ул. Рахова, д. 187/213
		Телефон/факс: +7 8452 674.550 (RU, Saratov) +7 499 705-91-01 (RU, Moscow)
		Банковские реквизиты: ПАО "ХАНТЫ-МАНСИЙСКИЙ БАНК ОТКРЫТИЕ" БИК 044583297
		р.с. 40702810114000022127 к.с. 30101810700000000297
		Email	info@heinzelmaennchenltd.com
		Генеральный директор	Андрей Анатольевич Яковлев
	"""
	worksheet.write(row, 2, headerText)
	row +=1
	
	# description
	col = 1
	desc = [u"Страна", u"Направление", u"Код страны", 
			u"Префикс", u"Начало", u"Конец", u"Цена, руб.", 
			u"Действительно от"]
	for item in desc:
		worksheet.write(row, col, item, headerFormat)
		col +=1
	row +=1

	# Data
	data = CoreOrganizations.objects.getTariffs(organization)
	for rowItem in data:
		worksheet.write(row, 1, rowItem.countryName)
		worksheet.write(row, 2, rowItem.name)
		worksheet.write(row, 3, rowItem.countryITU)
		worksheet.write(row, 4, rowItem.prefix)
		worksheet.write(row, 5, rowItem.numberFrom)
		worksheet.write(row, 6, rowItem.numberTill)
		worksheet.write(row, 7, rowItem.price)
		worksheet.write_datetime(row, 8, rowItem.dateFrom, date_format)
		row += 1

	workbook.close()
	
	if outputType:
		output.seek(0)
		return output
	else: 
		return output

def price(request, organization):

	from transliterate import translit, get_available_language_codes
	from time import time, strftime
	import re

	# TODO make func in models

	company = CoreOrganizations.objects.get(organizationId=organization)
	print company.shortName
	filename = "%s-%s.xlsx" % (re.sub('\W+','', translit(company.shortName, 'ru', reversed=True) ), strftime("%Y-%m-%d"))
	print filename

	response = HttpResponse(generatePrice(organization).read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	response['Content-Disposition'] = "attachment; filename="+filename

	return response