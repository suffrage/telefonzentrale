#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from django.shortcuts import render

from django.http import HttpResponse
from django.template import RequestContext, loader
from models import *

from django.http import *
from django.shortcuts import render_to_response,redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse

from viewsXls import *


def testView(request):
	template = loader.get_template('base.html')
	context = RequestContext(request, {
		'test': 'test',
	})
	return HttpResponse(template.render(context))

def directions(request):

	from voip.models import VoipDirection, VoipDirectionCodes
	from dir.models import DirCountry
	from django.db import connections
	cursor = connections['default'].cursor()

	query = """
	SELECT `voip_direction`.`directionId`, `name`, `countryName`, `countryITU`, codeList, `validFrom`, `validTill` FROM `voip_direction` 
	INNER JOIN `dir_country` ON ( `voip_direction`.`countryId` = `dir_country`.`countryId` ) 
	INNER JOIN (SELECT *,GROUP_CONCAT(`prefix`) as codeList FROM `voip_directionCodes` GROUP BY directionId) as codes on codes.`directionId` = `voip_direction`.`directionId`
	WHERE `validFrom` < CURRENT_TIMESTAMP() AND (`validTill` IS NULL OR `validTill` > CURRENT_TIMESTAMP())
	ORDER BY `voip_direction`.`name` ASC
	"""

	# cursor.execute(query)


	# directions = VoipDirection.objects.select_related('countryId').all().order_by('countryId__countryName').order_by('name')

	# directionCodes = VoipDirectioncodes.objects.values('directionid').annotate(emails=GroupConcat('VoipDirectioncodes__prefix'))

	# result = cursor.fetchall()
	# print result

	result = VoipDirection.objects.raw(query)
	print result
	for p in result:
		print p.__dict__

	template = loader.get_template('directions.html')
	context = RequestContext(request, {
		'test': 'test',
		'directions': result,
		'page': 'directions',
	})
	return HttpResponse(template.render(context))

@login_required(login_url='/lk/login')
def dashboard(request, customer):

	from voip.models import VoipCdr
	from core.models import CoreCustomers

	template = loader.get_template('dashboard.html')
	context = RequestContext(request, {
		'test': 'test',
		'title': 'Панель управления',
		'page': 'dashboard',
	})
	return HttpResponse(template.render(context))

@login_required(login_url='/lk/login')
def clients(request):

	from core.models import CoreOrganizations

	organizations = CoreOrganizations.objects.all()

	template = loader.get_template('clients.html')
	context = RequestContext(request, {
		'test': 'test',
		'title': 'Клиенты',
		'page': 'clients',
		'organizations': organizations,
	})
	return HttpResponse(template.render(context))

@login_required(login_url='lk/login')
def cdr(request, customer):

	from voip.models import VoipCdr
	from core.models import CoreCustomers

	#TODO добавить 
	customerObj = CoreCustomers.objects.get(customerid=customer)
	print customerObj.shortname
	cdrsObj = VoipCdr.objects.filter(a=customerObj).order_by('-callDate')[:5]
	for i in cdrsObj:
		print i.recordId

	template = loader.get_template('cdr.html')
	context = RequestContext(request, {
		'test': 'test',
		'cdrs': cdrsObj,
		'page': 'cdr',
	})
	return HttpResponse(template.render(context))



@login_required(login_url='lk/login')
def companies(request):

	from core.models import CoreOrganizations

	companies = CoreOrganizations.objects.all()

	template = loader.get_template('companies.html')
	context = RequestContext(request, {
		'test': 'test',
		'companies': companies,
		'page': 'companies',
	})
	return HttpResponse(template.render(context))


@login_required(login_url='lk/login')
def company(request, organization):

	from core.models import CoreOrganizations

	company = CoreOrganizations.objects.select_related('bankAccountIdForInvoice', 'cityId', 'bankAccountIdForInvoice__bankId').get(organizationId=organization)
	tariffs = CoreOrganizations.objects.getTariffs(1)

	template = loader.get_template('company.html')
	context = RequestContext(request, {
		'test': 'test',
		'company': company,
		'tariffs': tariffs,
		'page': 'companies',
	})
	return HttpResponse(template.render(context))



def requestConnect(request):

	if request.method == 'POST': # If the form has been submitted...
		
		form = RequestConnectForm(request.POST) # A form bound to the POST data
		if form.is_valid(): # All validation rules pass
			# Process the data in form.cleaned_data
			# ...
			return HttpResponseRedirect('/thanks/') # Redirect after POST
	else:
		form = RequestConnectForm() # An unbound form

	return render(request, 'request.html', {
		'form': form,
	})


def loginView(request):

	if request.user.is_authenticated():
		return HttpResponseRedirect(reverse('lk.views.dashboard', args=('1',)))

	error = ''

	if request.method == 'POST': # If the form has been submitted...
		username = request.POST['username']
		password = request.POST['password']

		user = authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				login(request, user)
				return HttpResponseRedirect(reverse('lk.views.dashboard', args=('1',)))
			else:
				error = "Ваш аккаунт не активен, обратитесь в техническую поддержку"
		else:
			error = "Неверный логин или пароль"
		

	template = loader.get_template('login.html')
	context = RequestContext(request, {
		'test': 'test',
		'error': error
	})
	return HttpResponse(template.render(context))


	# template = loader.get_template('request.html')
	# context = RequestContext(request, {
	# 	'test': 'test',
	# })
	# return HttpResponse(template.render(context))

def logoutView(request):
    logout(request)
    return HttpResponseRedirect(reverse('lk.views.loginView'))