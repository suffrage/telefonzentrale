from tastypie.resources import ModelResource
from core.models import *


class OrganizationsResource(ModelResource):
    class Meta:
        queryset = CoreOrganizations.objects.all()
        resource_name = 'organizations'