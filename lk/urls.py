#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url

from lk import views
from lk import viewsXls

app_name = 'lk'

urlpatterns = patterns('',
    url(r'^testView/$', views.testView, name='testView'),
    url(r'^price/(?P<organization>\d+)/$', viewsXls.price, name='price'),
    url(r'^directions/$', views.directions, name='directions'),
    url(r'^companies/$', views.companies, name='companies'),
    url(r'^companies/(?P<organization>\d+)/$', views.company, name='company'),
    url(r'^cdr/(?P<customer>\d+)/$', views.cdr, name='cdr'),
    url(r'^dashboard/(?P<customer>\d+)/$', views.dashboard, name='dashboard'),
    url(r'^clients/$', views.clients, name='clients'),
    url(r'^request/$', views.requestConnect, name='requestConnect'),
    url(r'^login/$', views.loginView, name='loginView'),
    url(r'^logout/$', views.logoutView, name='logoutView'),
)