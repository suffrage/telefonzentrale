#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from django.db import models
from django.forms import ModelForm
from django.utils.translation import ugettext_lazy as _


# class Menu(models.Model):


PT = (
		("advance", "Аванс"),
		("credit", "Кредит"),
	)

CT = (
		("ip", "IP"),
		("sipid", "SIP ID"),
	)

class RequestConnect(models.Model):
	paymentType = models.CharField(db_column='paymentType', max_length=255, choices=PT)
	phoneNumber = models.CharField(db_column='phoneNumber', max_length=255)
	emailPrimary = models.EmailField(db_column='emailPrimary')
	ips = models.TextField(db_column='ips') 
	connectionType = models.CharField(db_column='connectionType', max_length=255, choices=CT)
	details = models.TextField(db_column='details')
	fullName = models.CharField(db_column='fullName', max_length=255)
	reasonId = models.ForeignKey('dir.DirReason', db_column='reasonId')

	class Meta:
		verbose_name = 'Заявка'
		verbose_name_plural = 'Заявки'


	def __str__(self):
		return self.fullName


   

class RequestConnectForm(ModelForm):
    class Meta:
        model = RequestConnect
        fields = '__all__'
        labels = {
            'phoneNumber': _('Номер телефона'),
            'paymentType': _('Тип оплаты Ter'),
        }

    def __init__(self, *args, **kwargs):
        super(RequestConnectForm, self).__init__(*args, **kwargs)
        self.fields['phoneNumber'].widget.attrs.update({'class' : 'form-control', 'placeholder': 'Номер телефона'})
        self.fields['paymentType'].widget.attrs.update({'class' : 'form-control m-b'})
        

# 3 Основание действия подписанта Устав, доверенность (предусмотрено поле 

# 7 Выбор услуг Список услуг выбирается галочками. 

# 8 Выбор тарифного плана Предвыбор из списка тарифных планов 

# 9 Пропуск А-номера Да, нет, если да – какой номер/номера, 

# 12 Заказ оборудования Да, нет, если да – предвыбор типа 

# 13 Заказ Asterisk*