from django.contrib import admin

from models import *

admin.site.register(VoipCdr)
admin.site.register(VoipCdrFromFile)
admin.site.register(VoipCdrFs)
admin.site.register(VoipCustomersGateways)
admin.site.register(VoipDirection)
admin.site.register(VoipDirectionCodes)
admin.site.register(VoipDirectionPriceBuy)
admin.site.register(VoipDirectionPriceSell)
admin.site.register(VoipModificators)
admin.site.register(VoipProfiles)
admin.site.register(VoipRouteTypes)
admin.site.register(VoipTarifs)
admin.site.register(VoipTarifsMap)