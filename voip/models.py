from django.db import models

class VoipCdr(models.Model):
    recordId = models.BigIntegerField(db_column='recordId', primary_key=True)  # Field name made lowercase.
    callDate = models.DateTimeField(db_column='callDate')  # Field name made lowercase.
    a_num = models.CharField(max_length=32)
    b_num = models.CharField(max_length=32)
    billDec = models.BigIntegerField(db_column='billSec')  # Field name made lowercase.
    connectTime = models.DateTimeField(db_column='connectTime')  # Field name made lowercase.
    endTime = models.DateTimeField(db_column='endTime')  # Field name made lowercase.
    a_ip = models.CharField(max_length=15)
    b_ip = models.CharField(max_length=15)
    a = models.ForeignKey('core.CoreCustomers', related_name='a')
    b = models.ForeignKey('core.CoreCustomers', related_name='b')
    billSecBuy = models.IntegerField(db_column='billSecBuy', blank=True, null=True)  # Field name made lowercase.
    billSecSell = models.IntegerField(db_column='billSecSell', blank=True, null=True)  # Field name made lowercase.
    sellPrice = models.DecimalField(db_column='sellPrice', max_digits=20, decimal_places=10, blank=True, null=True)  # Field name made lowercase.
    sellSum = models.DecimalField(db_column='sellSum', max_digits=20, decimal_places=10, blank=True, null=True)  # Field name made lowercase.
    buyPrice = models.DecimalField(db_column='buyPrice', max_digits=20, decimal_places=10, blank=True, null=True)  # Field name made lowercase.
    buySum = models.DecimalField(db_column='buySum', max_digits=20, decimal_places=10, blank=True, null=True)  # Field name made lowercase.
    profit = models.DecimalField(max_digits=20, decimal_places=10, blank=True, null=True)
    q850 = models.IntegerField()
    reason = models.CharField(max_length=32)
    directionId = models.ForeignKey('VoipDirection', db_column='directionId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'voip_cdr'


class VoipCdrFromFile(models.Model):
    recordId = models.BigIntegerField(db_column='recordId', primary_key=True)  # Field name made lowercase.
    devId = models.CharField(db_column='devId', max_length=255)  # Field name made lowercase.
    connectTime = models.DateTimeField(db_column='connectTime')  # Field name made lowercase.
    billSec = models.BigIntegerField(db_column='billSec')  # Field name made lowercase.
    q850 = models.IntegerField()
    reason = models.CharField(max_length=32)
    a_ip = models.CharField(max_length=15)
    a_type = models.CharField(max_length=10)
    a_abonentName = models.CharField(db_column='a_abonentName', max_length=255)  # Field name made lowercase.
    a_num_in = models.CharField(max_length=32)
    a_num_out = models.CharField(max_length=32)
    redirect_num = models.CharField(max_length=32)
    b_ip = models.CharField(max_length=15)
    b_type = models.CharField(max_length=10)
    b_abonentName = models.CharField(db_column='b_abonentName', max_length=255)  # Field name made lowercase.
    b_num_in = models.CharField(max_length=32)
    b_num_out = models.CharField(max_length=32)
    startTime = models.DateTimeField(db_column='startTime')  # Field name made lowercase.
    endTime = models.DateTimeField(db_column='endTime')  # Field name made lowercase.
    redirectType = models.CharField(db_column='redirectType', max_length=11)  # Field name made lowercase.
    addTime = models.DateTimeField(db_column='addTime')  # Field name made lowercase.
    cdrId = models.BigIntegerField(db_column='cdrId', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'voip_cdr_fromfile'


class VoipCdrFs(models.Model):
    recordId = models.BigIntegerField(db_column='recordId', primary_key=True)  # Field name made lowercase.
    devid = models.CharField(db_column='devId', max_length=255)  # Field name made lowercase.
    connectTime = models.DateTimeField(db_column='connectTime')  # Field name made lowercase.
    billSec = models.BigIntegerField(db_column='billSec')  # Field name made lowercase.
    q850 = models.IntegerField()
    reason = models.CharField(max_length=32, blank=True, null=True)
    a_ip = models.CharField(max_length=15)
    a_type = models.CharField(max_length=10)
    a_abonentName = models.CharField(db_column='a_abonentName', max_length=255)  # Field name made lowercase.
    a_num_in = models.CharField(max_length=32)
    a_num_out = models.CharField(max_length=32)
    redirect_num = models.CharField(max_length=32)
    b_ip = models.CharField(max_length=15)
    b_type = models.CharField(max_length=10)
    b_abonentName = models.CharField(db_column='b_abonentName', max_length=255)  # Field name made lowercase.
    b_num_in = models.CharField(max_length=32)
    b_num_out = models.CharField(max_length=32)
    startTime = models.DateTimeField(db_column='startTime')  # Field name made lowercase.
    endTime = models.DateTimeField(db_column='endTime')  # Field name made lowercase.
    redirectType = models.CharField(db_column='redirectType', max_length=11)  # Field name made lowercase.
    addTime = models.DateTimeField(db_column='addTime')  # Field name made lowercase.
    cdrId = models.BigIntegerField(db_column='cdrId', blank=True, null=True)  # Field name made lowercase.
    uuid = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = 'voip_cdr_fs'


class VoipCustomersGateways(models.Model):
    gatewayId = models.BigIntegerField(db_column='gatewayId', primary_key=True)  # Field name made lowercase.
    customerId = models.ForeignKey('core.CoreCustomers', db_column='customerId')  # Field name made lowercase.
    ip = models.CharField(unique=True, max_length=15)
    port = models.IntegerField()
    gatewayType = models.CharField(max_length=10)
    codec = models.CharField(max_length=16)
    enabled = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'voip_customers_gateways'


class VoipDirection(models.Model):
    directionId = models.AutoField(db_column='directionId', primary_key=True)  # Field name made lowercase.
    countryId = models.ForeignKey('dir.DirCountry', db_column='countryId')  # Field name made lowercase.
    name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'voip_direction'


class VoipDirectionCodes(models.Model):
    directionCodeId = models.AutoField(db_column='directionCodeId', primary_key=True)  # Field name made lowercase.
    directionId = models.ForeignKey(VoipDirection, db_column='directionId')  # Field name made lowercase.
    prefix = models.CharField(max_length=15)
    numberFrom = models.IntegerField(db_column='numberFrom', blank=True, null=True)  # Field name made lowercase.
    numberTill = models.IntegerField(db_column='numberTill', blank=True, null=True)  # Field name made lowercase.
    validFrom = models.DateField(db_column='validFrom')  # Field name made lowercase.
    validTill = models.DateField(db_column='validTill', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'voip_directionCodes'


class VoipDirectionPriceBuy(models.Model):
    directionpriceId = models.AutoField(db_column='directionPriceId', primary_key=True)  # Field name made lowercase.
    directionId = models.ForeignKey(VoipDirection, db_column='directionId')  # Field name made lowercase.
    customerId = models.ForeignKey('core.CoreCustomers', db_column='customerId')  # Field name made lowercase.
    billingIncrement = models.IntegerField(db_column='billingIncrement')  # Field name made lowercase.
    price = models.DecimalField(max_digits=20, decimal_places=10)
    lock = models.IntegerField()
    dateFrom = models.DateField(db_column='dateFrom')  # Field name made lowercase.
    directionType = models.IntegerField(blank=True, null=True)
    profileId = models.ForeignKey('VoipProfiles', db_column='profileId', blank=True, null=True)  # Field name made lowercase.
    quality = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'voip_directionPriceBuy'



class VoipDirectionPriceSell(models.Model):
    directionPriceId = models.AutoField(db_column='directionPriceId', primary_key=True)  # Field name made lowercase.
    directionId = models.ForeignKey(VoipDirection, db_column='directionId')  # Field name made lowercase.
    tarifId = models.ForeignKey('VoipTarifs', db_column='tarifId', blank=True, null=True)  # Field name made lowercase.
    customerId = models.ForeignKey('core.CoreCustomers', db_column='customerId', blank=True, null=True)  # Field name made lowercase.
    billingIncrement = models.IntegerField(db_column='billingIncrement')  # Field name made lowercase.
    price = models.DecimalField(max_digits=20, decimal_places=10, blank=True, null=True)
    dateFrom = models.DateField(db_column='dateFrom')  # Field name made lowercase.
    directionType = models.IntegerField(blank=True, null=True)
    
    class Meta:
        managed = False
        db_table = 'voip_directionPriceSell'


class VoipModificators(models.Model):
    modificatorId = models.BigIntegerField(db_column='modificatorId', primary_key=True)  # Field name made lowercase.
    modilficatorName = models.CharField(db_column='modilficatorName', max_length=255)  # Field name made lowercase.
    modilficatorPattern = models.CharField(db_column='modilficatorPattern', max_length=255)  # Field name made lowercase.
    modilficatorReplacement = models.CharField(db_column='modilficatorReplacement', max_length=255)  # Field name made lowercase.
    modilficatorFlag = models.IntegerField(db_column='modilficatorFlag')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'voip_modificators'


class VoipProfiles(models.Model):
    profileId = models.AutoField(db_column='profileId', primary_key=True)  # Field name made lowercase.
    profileName = models.CharField(db_column='profileName', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'voip_profiles'


class VoipRouteTypes(models.Model):
    routeTypeId = models.BigIntegerField(db_column='routeTypeId', primary_key=True)  # Field name made lowercase.
    routeTypeName = models.CharField(db_column='routeTypeName', max_length=255)  # Field name made lowercase.
    routeTypeDescription = models.CharField(db_column='routeTypeDescription', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'voip_routeTypes'


class VoipTarifs(models.Model):
    tarifid = models.AutoField(db_column='tarifId', primary_key=True)  # Field name made lowercase.
    description = models.CharField(max_length=32)

    class Meta:
        managed = False
        db_table = 'voip_tarifs'


class VoipTarifsMap(models.Model):
    tarifsmapId = models.AutoField(db_column='tarifsMapId', primary_key=True)  # Field name made lowercase.
    customerId = models.IntegerField(db_column='customerId')  # Field name made lowercase.
    tarifId = models.IntegerField(db_column='tarifId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'voip_tarifsMap'