SELECT custA.shortName as A, custB.shortName as B, countCall, 
	`voip_direction`.name, asr1, asr2, asr3, cdr.code16, cdr.code17, cdr.codeOther, cdr.acd, countNotNull, sumBillSec

FROM 
(SELECT a_id, b_id, directionId, q850, 
	count(*) as countCall,
	SUM(CASE WHEN billSec>0 THEN 1 ELSE 0 END) as countNotNull, 
	SUM(CASE WHEN billSec>0 THEN 1 ELSE 0 END) / count(*) as asr1,
	( SUM(CASE WHEN q850=16 THEN 1 ELSE 0 END) + SUM(CASE WHEN q850=17 THEN 1 ELSE 0 END) ) / count(*) as asr2,
	SUM(CASE WHEN q850=16 THEN 1 ELSE 0 END) / count(*) as asr3,
	SUM(CASE WHEN q850=16 THEN 1 ELSE 0 END) as code16,
	SUM(CASE WHEN q850=17 THEN 1 ELSE 0 END) as code17,
	SUM(CASE WHEN q850 NOT IN (17,16) THEN billSec ELSE 0 END) as codeOther,
	SUM(CASE WHEN billSec>0 THEN billSec ELSE 0 END) / SUM(CASE WHEN billSec>0 THEN 1 ELSE 0 END)/60 AS acd

FROM `voip_cdr` WHERE endTime > DATE_SUB(now(), INTERVAL 60*24 MINUTE) GROUP BY a_id, directionId, b_id) as cdr
INNER JOIN `core_customers` as custA ON a_id = custA.customerId
INNER JOIN `core_customers` as custB ON b_id = custB.customerId
INNER JOIN `voip_direction` ON cdr.`directionId` = `voip_direction`.`directionId`


