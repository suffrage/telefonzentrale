#!/usr/bin/python
# -*- coding: utf-8 -*-

import MySQLdb as mdb
import sys, re, time, socket
from transliterate import translit, get_available_language_codes

server = ('188.120.226.161', 2003)

def sendGraph():
	# try:
	con = mdb.connect('localhost', 'root', '291312', 'billing');

	cur = con.cursor(mdb.cursors.DictCursor)
	cur.execute("SELECT VERSION()")
	cur.execute('SET NAMES utf8')
	cur.execute('SET CHARACTER SET utf8')
	cur.execute('SET COLLATION_CONNECTION="utf8_general_ci"')

	ver = cur.fetchone()

	print "Database version : %s " % ver

	sql = """
		SELECT 
			custA.shortName as A, 
			custB.shortName as B, 
			countCall, 
			`voip_direction`.name, 
			countryName, 
			cdr.codeOther, 
			countNotNull, 
			sumBillSec, 
			countBillSec,

			sumBillSec/countBillSec/60 as acd,
			countBillSec/countCall as asr1,
			(countCode16+countCode17)/countCall as asr2,
			countCode16/countCall as asr3

		FROM 
		(SELECT a_id, b_id, directionId, q850, 
		count(*) as countCall,
		SUM(CASE WHEN q850=16 THEN 1 ELSE 0 END) as countCode16,
		SUM(CASE WHEN q850=17 THEN 1 ELSE 0 END) as countCode17,
		SUM(CASE WHEN billSec>0 THEN 1 ELSE 0 END) as countNotNull, 
		SUM(CASE WHEN q850 NOT IN (17,16) THEN billSec ELSE 0 END) as codeOther,
		SUM(CASE WHEN billSec>0 THEN billSec ELSE 0 END) as sumBillSec,
		SUM(CASE WHEN billSec>0 THEN 1 ELSE 0 END) as countBillSec

		FROM `voip_cdr` WHERE 
		a_id <> b_id AND
		endTime > DATE_SUB(now(), INTERVAL 60*24*60 MINUTE) GROUP BY a_id, directionId, b_id) as cdr
		INNER JOIN `core_customers` as custA ON a_id = custA.customerId
		INNER JOIN `core_customers` as custB ON b_id = custB.customerId
		INNER JOIN `voip_direction` ON cdr.`directionId` = `voip_direction`.`directionId`
		INNER JOIN `dir_country` ON `voip_direction`.`countryId` = `dir_country`.`countryId`
	"""


	cur.execute(sql)
	curTime = int(time.time())

	def toTranslit(stringToTranslit):
		return re.sub('\W+','', translit(stringToTranslit.decode('utf-8'), 'ru', reversed=True) )

	sock = socket.socket()
	sock.connect(server)

	for item in  cur.fetchall():
		#print toTranslit(item['name'])
		
		graph = []

		graph.append("heinzelmaennchenltd.in.%s.%s.%s.%s.asr1 %s %d\n" %(item['A'], toTranslit(item['countryName']), toTranslit(item['name']), item['B'], item['asr1'], curTime))
		graph.append("heinzelmaennchenltd.in.%s.%s.%s.%s.asr2 %s %d\n" %(item['A'], toTranslit(item['countryName']), toTranslit(item['name']), item['B'], item['asr2'], curTime))
		graph.append("heinzelmaennchenltd.in.%s.%s.%s.%s.asr3 %s %d\n" %(item['A'], toTranslit(item['countryName']), toTranslit(item['name']), item['B'], item['asr3'], curTime))
		graph.append("heinzelmaennchenltd.in.%s.%s.%s.%s.acd %s %d\n" %(item['A'], toTranslit(item['countryName']), toTranslit(item['name']), item['B'], item['acd'], curTime))



		for i in graph:
			print i
			# sock.sendall(i)

	sock.close()

	con.close()

while True:
	sendGraph()
	print "\n sleeping... \n"
	time.sleep(30)