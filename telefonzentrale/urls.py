from django.conf.urls import include, url
from django.contrib import admin

from tastypie.api import Api
from lk.api import *

v1_api = Api(api_name='v1')
v1_api.register(OrganizationsResource())

urlpatterns = [
    # Examples:
    # url(r'^$', 'telefonzentrale.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^lk/', include('lk.urls', namespace="lk", app_name="lk")),
    url(r'^api/', include(v1_api.urls)),
]
