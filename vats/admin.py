from django.contrib import admin

from models import *

admin.site.register(VatsConditions)
admin.site.register(VatsContexts)
admin.site.register(VatsExtensions)
admin.site.register(VatsProfiles)