#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from django.db import models

class VatsConditions(models.Model):
    conditionId = models.AutoField(db_column='conditionId', primary_key=True)  # Field name made lowercase.
    extensionId = models.ForeignKey('VatsExtensions', db_column='extensionId')  # Field name made lowercase.
    field = models.CharField(max_length=255)
    expression = models.CharField(max_length=255)
    require_nested = models.IntegerField(db_column='require-nested')  # Field renamed to remove unsuitable characters.
    break_field = models.CharField(db_column='break', max_length=8)  # Field renamed because it was a Python reserved word.

    class Meta:
        managed = False
        db_table = 'vats_conditions'


class VatsContexts(models.Model):
    contextid = models.AutoField(db_column='contextId', primary_key=True)  # Field name made lowercase.
    name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'vats_contexts'


class VatsExtensions(models.Model):
    extensionid = models.AutoField(db_column='extensionId', primary_key=True)  # Field name made lowercase.
    contextid = models.ForeignKey(VatsContexts, db_column='contextId')  # Field name made lowercase.
    name = models.CharField(max_length=255)
    continue_field = models.IntegerField(db_column='continue')  # Field renamed because it was a Python reserved word.
    order = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'vats_extensions'


class VatsProfiles(models.Model):
    profileid = models.AutoField(db_column='profileId', primary_key=True)  # Field name made lowercase.
    name = models.CharField(max_length=255)
    externalIp = models.CharField(db_column='externalIp', max_length=15)  # Field name made lowercase.
    sipPort = models.IntegerField(db_column='sipPort')  # Field name made lowercase.
    tls = models.IntegerField()
    tlsPort = models.IntegerField(db_column='tlsPort', blank=True, null=True)  # Field name made lowercase.
    tlsOnly = models.IntegerField(db_column='tlsOnly')  # Field name made lowercase.
    contextId = models.ForeignKey(VatsContexts, db_column='contextId', blank=True, null=True)  # Field name made lowercase.
    defaultCli = models.CharField(db_column='defaultCli', max_length=15)  # Field name made lowercase.
    enabled = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'vats_profiles'
        unique_together = (('externalIp', 'sipPort'),)
