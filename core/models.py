#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from django.db import models

class CoreAccounts(models.Model):
    accountId = models.AutoField(db_column='accountId', primary_key=True)  # Field name made lowercase.
    ballance = models.DecimalField(max_digits=20, decimal_places=5)
    limit = models.DecimalField(max_digits=20, decimal_places=5)
    description = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'core_accounts'

    # def __unicode__(self):
    #     return self.accountId

class CoreBankAccounts(models.Model):
    bankAccountId = models.AutoField(db_column='bankAccountId', primary_key=True)  # Field name made lowercase.
    organizationId = models.ForeignKey('CoreOrganizations', db_column='organizationId')  # Field name made lowercase.
    bankAccountNumber = models.CharField(db_column='bankAccountNumber', max_length=255)  # Field name made lowercase.
    bankId = models.ForeignKey('dir.DirBanks', db_column='bankId')  # Field name made lowercase.
    activeFrom = models.DateField(db_column='activeFrom')  # Field name made lowercase.
    activeTill = models.DateField(db_column='activeTill', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'core_bankAccounts'

    def __unicode__(self):
        return "%s - %s" % (self.bankAccountNumber, self.organizationId)



class CoreCustomers(models.Model):
    customerid = models.AutoField(db_column='customerId', primary_key=True)  # Field name made lowercase.
    shortname = models.CharField(unique=True, max_length=255)
    enabled = models.IntegerField()
    createdate = models.DateField(db_column='createDate')  # Field name made lowercase.
    managerid = models.IntegerField(db_column='managerId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'core_customers'

    # def __unicode__(self):
    #     return self.shortname


class CoreMapAccount2Customers(models.Model):
    accountid = models.ForeignKey(CoreAccounts, db_column='accountId')  # Field name made lowercase.
    customerid = models.ForeignKey(CoreCustomers, db_column='customerId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'core_map_account2customers'

    # def __unicode__(self):
    #     return self.accountid


class CoreMapOrganization2Customer(models.Model):
    organizationid = models.ForeignKey('CoreOrganizations', db_column='organizationId')  # Field name made lowercase.
    customerid = models.ForeignKey(CoreCustomers, db_column='customerId')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'core_map_organization2customer'

    # def __unicode__(self):
    #     return self.organizationid.shortname, ' - ', self.customerid.shortname


class CoreOrganizationsManager(models.Manager):
    def getTariffs(self, organizationId):
        
        # TODO time.strftime('%Y-%m-%d %H:%M:%S') -> CURRENT_TIMESTAMP

        from voip.models import VoipDirectionPriceSell
        from voip.models import VoipTarifsMap
        import time

        try:
           tariffId = VoipTarifsMap.objects.get(customerId=organizationId).tarifId
        except VoipTarifsMap.DoesNotExist:
           tariffId = "Null"

        currentTimeStamp = str(time.strftime('%Y-%m-%d %H:%M:%S'))
        print currentTimeStamp


        query = """
                SELECT * FROM 
                    (
                        SELECT directionPriceId, countryName, name, countryITU, prefix, numberFrom, numberTill, price, dateFrom FROM
                        ( 
                            SELECT * FROM 
                            (
                                (
                                    SELECT directionPriceId, directionId, tarifId, customerId, (IFNULL(tarifId,0) + IFNULL(customerId,0)*1000) as weight, billingIncrement, price, dateFrom, type FROM
                                        (
                                            SELECT * FROM `voip_directionPriceSell` WHERE
                                                `voip_directionPriceSell`.`dateFrom` < '%s' AND
                                                (
                                                    (`voip_directionPriceSell`.`tarifId` IS NULL  AND `voip_directionPriceSell`.`customerId` IS NULL) OR 
                                                    (`voip_directionPriceSell`.`tarifId` = %s AND `voip_directionPriceSell`.`customerId` IS NULL) OR
                                                    (`voip_directionPriceSell`.`customerId` = %s AND `voip_directionPriceSell`.`tarifId` IS NULL)
                                                ) 
                                                ORDER BY `voip_directionPriceSell`.`dateFrom` DESC
                                        ) 
                                        as dps 
                                        GROUP BY directionId, tarifId, customerId
                                        ORDER BY weight DESC 
                                ) as tmp
                            ) GROUP BY directionId
                        ) as tarif
                        INNER JOIN `voip_direction` ON `voip_direction`.`directionId` = tarif.`directionId`
                        INNER JOIN `voip_directionCodes` ON `voip_directionCodes`.`directionId` = tarif.`directionId`
                        INNER JOIN `dir_country` ON `voip_direction`.`countryId` = `dir_country`.`countryId`
                        WHERE '%s' BETWEEN `validFrom` AND IFNULL(`validTill`, '%s')
                    ) as result ORDER BY countryITU, prefix, numberFrom, numberTill;
                """ % (currentTimeStamp, tariffId, organizationId, currentTimeStamp, currentTimeStamp)
        
        tariffs = VoipDirectionPriceSell.objects.raw(query)

        return tariffs


class CoreOrganizations(models.Model):
    organizationId = models.AutoField(db_column='organizationId', primary_key=True)  # Field name made lowercase.
    vat = models.CharField(max_length=25)
    kpp = models.CharField(max_length=255)
    fullName = models.CharField(db_column='fullName', max_length=255)  # Field name made lowercase.
    shortName = models.CharField(db_column='shortName', max_length=255)  # Field name made lowercase.
    cityId = models.ForeignKey('dir.DirCity', db_column='cityId')  # Field name made lowercase.
    address1 = models.CharField(max_length=255)
    address2 = models.CharField(max_length=255)
    zip = models.CharField(max_length=25)
    phone1 = models.CharField(max_length=25, blank=True, null=True)
    phone2 = models.CharField(max_length=25, blank=True, null=True)
    bankAccountIdForInvoice = models.ForeignKey(CoreBankAccounts, db_column='bankAccountIdForInvoice', blank=True, null=True)  # Field name made lowercase.
    manager1roleId = models.IntegerField(db_column='manager1RoleId', blank=True, null=True)  # Field name made lowercase.
    manager2roleId = models.IntegerField(db_column='manager2RoleId', blank=True, null=True)  # Field name made lowercase.
    manager1personId = models.IntegerField(db_column='manager1PersonId', blank=True, null=True)  # Field name made lowercase.
    manager2personId = models.IntegerField(db_column='manager2PersonId', blank=True, null=True)  # Field name made lowercase.
    objects = CoreOrganizationsManager()

    class Meta:
        managed = False
        db_table = 'core_organizations'

    def __unicode__(self):
        return self.shortname