#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from django.contrib import admin

from models import *

admin.site.register(CoreAccounts)

class CoreBankAccountsAdmin(admin.ModelAdmin):
	list_display = ["bankAccountNumber", "organizationId", "bankId", 
					"activeFrom", "activeTill"]

admin.site.register(CoreBankAccounts, CoreBankAccountsAdmin)
admin.site.register(CoreCustomers)
admin.site.register(CoreMapAccount2Customers)
admin.site.register(CoreMapOrganization2Customer)
admin.site.register(CoreOrganizations)